import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import java.io.File;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.Map;
 
public class Graph {
    private int numVertices;
    private int numEdges;
 
    public Graph() {
        numVertices = 0;
        numEdges = 0;
    }
    
    HashMap<String,List> adjListsMap = new HashMap<String,List>();
 
    public int getNumVertices() {
        return numVertices;
    }
 
    public int getNumEdges() {
        return numEdges;
    }
 
 
    public void setNumVertices(int v) {
        numVertices = v;
    }
 
    public void setNumEdges(int e) {
        numEdges = e;
    }
    
    public  void addVertex(String vertex) {  //put vertex in map
    int v = getNumVertices();
    ArrayList<String> neighbors = new ArrayList<String>();
    adjListsMap.put(vertex, neighbors);
    setNumVertices(v+1);
}
    
   
    public void removeVertex()  {
    // Remove the vertex at the end
    int numV = getNumVertices();
    if (numV == 0)
    adjListsMap.remove(numV);
    setNumVertices(numV+1);
}
    //add edge to show relationship
    public void addEdge(String v, String w) {
  int numV = getNumVertices();
 
    (adjListsMap.get(v)).add(w);
    setNumEdges(getNumEdges()+1);
}
    //get neighbours of vertex
    public List<String> getNeighbors(String v){
  int numV = getNumVertices();

  
    List<String> neighbors = new ArrayList<String>();
    for (Object x : adjListsMap.get(v)) {
      
      neighbors.add(x.toString());
    }
    
    return neighbors;
}
    
   

   public void saveMaps() {
     try
           {
                  FileOutputStream fos =
                     new FileOutputStream("graph.ser");
                  ObjectOutputStream oos = new ObjectOutputStream(fos);
                  oos.writeObject(adjListsMap);
                  oos.close();
                  fos.close();
                  System.out.printf("Serialized HashMap data is saved in graph.ser");
           }catch(IOException ioe)
            {
                  ioe.printStackTrace();
            }
      }


public void loadMaps() {
   try
      {
         FileInputStream fis = new FileInputStream("graph.ser");
         ObjectInputStream ois = new ObjectInputStream(fis);
         adjListsMap = (HashMap) ois.readObject();
         ois.close();
         fis.close();
      }catch(IOException ioe)
      {
         ioe.printStackTrace();
         return;
      }catch(ClassNotFoundException c)
      {
         System.out.println("Class not found");
         c.printStackTrace();
         return;
      }
      System.out.println("Deserialized HashMap Graph..");
      // Display content using Iterator
      Set set = adjListsMap.entrySet();
      Iterator iterator = set.iterator();
      while(iterator.hasNext()) {
         Map.Entry mentry = (Map.Entry)iterator.next();
         System.out.print("key: "+ mentry.getKey() + " & Value: ");
         System.out.println(mentry.getValue());
      }
    }
}