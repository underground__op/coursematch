public class Course{
  private String name;
  private String grade;
  private String major;

  
  //ACCESSORS
  public String getName(){
    return name;
  }
  
  public String getGrade(){
    return grade;
  }
  
  public String getMajor(){
    return major;
  }
  
  //MUTATORS
  public void setGrade(String newGrade){
    grade = newGrade;
  }
  
  
  public void setName(String newName){
    name = newName;
  }
 
  
  //CONSTRUCTORS
  public Course(String newName, String grade){
    name = newName;
    this.grade = grade;
  }
  
  //BOOLEANS
  public boolean bothEquals(Course fresh){
    if(this.grade == fresh.grade && this.name.equals(fresh.name)){return true;}
    else{return false;}
  }
  
  public boolean nameEquals(Course fresh){
    if(this.name.equals(fresh.name)){return true;}
    else{return false;}
  }
  
  public boolean ageEquals(Course fresh){
    if(this.grade == fresh.grade){return true;}
    else{return false;}
  }

  
  
}