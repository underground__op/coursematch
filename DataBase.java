import java.util.HashMap;
import java.util.List;
import java.io.File;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.Map;
import java.util.ArrayList;

public class DataBase{
  
public File savedHashMaps = new File("SavedHashMaps.txt");  

public static  HashMap Database1 = new HashMap(); //create database of HashMap type

Graph Database2=new Graph(); //create database with Graph type

//print details of all students
public void print(){

 Student temp;
 
  for (Object key :Database1.keySet()){
    
  temp = (Student) Database1.get(key);
  
  System.out.println(" id :"+temp.id);
  
  System.out.println("Name :"+temp.fullName);
  
  System.out.println("Gpa :"+temp.gpa);
  
  System.out.println("major :"+temp.major);
  
  System.out.println("Course number :"+temp.numOfCourses);
  
   System.out.println("Crime count :"+temp.crimeCount);
   
   System.out.println("Course list:"+temp.courseList);
   
   System.out.println("year group :"+temp.yeargroup);
   
    System.out.println("           ");
  
  }
   
     
}

  //print info of one student
  public void print1(Student nStudent){

 Student temp;
 
 // for (Object key :Database1.keySet()){
    
  temp =(Student)  Database1.get(nStudent.fullName);
  
  temp.gradeCalc(temp);
  
  System.out.println(" id :"+temp.id);
  
  System.out.println("Name :"+temp.fullName);
  
  System.out.println("Gpa :"+temp.gpa);
  
  System.out.println("major :"+temp.major);
  
  System.out.println("Course number :"+temp.numOfCourses);
  
   System.out.println("Crime count :"+temp.crimeCount);
   
   System.out.println("Course list:"+temp.courseList);
   
   System.out.println("year group :"+temp.yeargroup);
   
     
//}

}
//add student to both databses
public boolean addStudent(Student nStudent){ //addstudent 


Database1.put(nStudent.fullName,nStudent);

Database2.addVertex(nStudent.fullName);

return true;
}

//remove student from HashMap 
public boolean removeStudent(String name){ // remove student
  
  if(Database1.containsKey(name)){
  Database1.remove(name);
  return true;
  }
  else{
  return false;
  }
}

//get name
public String getName(Student nStudent){ ///get names of them

 String holder;
  
 holder = nStudent.fullName;
  
 return holder ;
}

//get student type from string
public Student getObject(String nStudent){



Student car = (Student) Database1.get(nStudent);

return car;

}




//add relationship 
public void friend(Student nStudent,Student friend){

  Database2.addEdge(this.getName(nStudent),this.getName(friend));
}

//Show all realtionship
public List<String> showf(Student nStudent){
return Database2.getNeighbors(this.getName(nStudent));
}





//save map to file
public void saveMaps() {
     try
           {
                  FileOutputStream fos =
                     new FileOutputStream("hashmap.ser");
                  ObjectOutputStream oos = new ObjectOutputStream(fos);
                  oos.writeObject(Database1);
                  oos.close();
                  fos.close();
                  System.out.printf("Serialized HashMap data is saved in hashmap.ser");
           }catch(IOException ioe)
            {
                  ioe.printStackTrace();
            }
           Database2.saveMaps();
      }

//load file
public void loadMaps() {
   try
      {
         FileInputStream fis = new FileInputStream("hashmap.ser");
         ObjectInputStream ois = new ObjectInputStream(fis);
         Database1 = (HashMap) ois.readObject();
         ois.close();
         fis.close();
      }catch(IOException ioe)
      {
         ioe.printStackTrace();
         return;
      }catch(ClassNotFoundException c)
      {
         System.out.println("Class not found");
         c.printStackTrace();
         return;
      }
      System.out.println("Deserialized HashMap..");
      // Display content using Iterator
      Set set = Database1.entrySet();
      Iterator iterator = set.iterator();
      while(iterator.hasNext()) {
         Map.Entry mentry = (Map.Entry)iterator.next();
         System.out.print("key: "+ mentry.getKey() + " & Value: ");
         System.out.println(mentry.getValue());
      }
      Database2.loadMaps();
    }

//get deans list
public List<String> getDeansList(){
   List x =new ArrayList();
  for (Object key :Database1.keySet()){
    
  Student temp = (Student) Database1.get(key);
  
  if (temp.isOndeansList(temp)){
    
  x.add(temp.fullName);
  }
  

}

   return x;    

}

//get dangerlist for those with low Gpas
public List<String> getDangerList(){
   List m =new ArrayList();
  for (Object key :Database1.keySet()){
    
  Student temp = (Student) Database1.get(key);
  
  if (temp.isOndangerList(temp)){
    
  m.add(temp.fullName);
  }
  

}

   return m;    

}

}