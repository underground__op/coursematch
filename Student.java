//Student Class
import java.util.Random; 
import java.util.ArrayList;
import java.util.HashMap;
import java.io.*;
import java.util.List;
import java.util.Map;

public class Student implements java.io.Serializable{
  //Instance Variables for the student
  public String fullName;
  public String major;
  public int numOfCourses;
  public int yeargroup;
  public double gpa;
  public int id;
  public int crimeCount;
  public String password;
  
  public HashMap<String, String> courseList = new HashMap<String, String>();
  
  ArrayList<String> theList = new ArrayList<String>();
  ArrayList<String> theBadList = new ArrayList<String>();
  
  //values signifying the dean grade and the fail grade
  public static final double PASS_DEAN = 3.5;
  public static final double FAIL_DEAN = 2.0;
  
  
  //Constructor, whereby the various respective parameters given to the student. 
  public Student(String newName, int yeargroup,String m, String password){
    fullName = newName;
    major = m;
    this.yeargroup = yeargroup;
    id = generateId(yeargroup);
    crimeCount = 0;
    this.password = password; 
  }
  
  //Accessors, used to access information from the student created
  public String getName(){
    return fullName;
  }
  
  public int getYeargroup(){
    return yeargroup;
  }
  
  public int getCrimeCount(){
    return crimeCount;
  }
  
  public String getPassword(){
    return password;
  }
  
  public String getMajor() {
    return major;
  }
  
  public int getId() {
    return id;
  }
  
  public double getGPA() {
    return gpa;
  }
  
  public int getNumOfCourses(){
    return numOfCourses;
  }  
  
  public void getStudentCourse(){
    System.out.println(courseList);
  }
  
  public void getDangerlist(){
    System.out.println(theBadList);
  }
  
  public void getDeanslist(){
    System.out.println(theList);
  }
  
   
  
  //Mutators, these methods are used to set and change certain values.
  public void setCrime(int val){
    crimeCount = val;
  }
  
  public void increaseCrime(Student guy, int val){
    guy.setCrime(val);
  }
  
  public void setName(String newName){
    fullName = newName;
  }
  
  public void setMajor(String newMajor) {
    major = newMajor;
  }
  
  public void setId(int newID) {
    id = newID;
  }
  
  public void setCourse(String myCourse, String grade){
    
    for (Map.Entry<String, String> entry : courseList.entrySet()) {
      String key = entry.getKey();
      String value = entry.getValue();
      
      if (key.equalsIgnoreCase(myCourse)){
        courseList.remove(key);
        
        courseList.put(myCourse, grade);
      }
      else{
        courseList.put(myCourse, grade);
        numOfCourses++;
      }
    }
  }
  
  public void addCourse(String myCourse,String grade){
    courseList.put(myCourse,grade);
    numOfCourses++;
  }   
  
  public void addCourse(String myCourse){
    courseList.put(myCourse,"NA");
    numOfCourses++;
  }
  
  public void updateGPA(double classGrade) {
    gpa = (numOfCourses*gpa + classGrade)/(numOfCourses+1);
    numOfCourses++;
  }
  
  //Booleans

  // This method is for checking whether the student has gone through a course, and whether the grade point is above the pass grade point.  
  public boolean isOndeansList(Student guy) {
    this.gradeCalc(guy);
    if (guy.getGPA() > PASS_DEAN && guy.getNumOfCourses() > 0){
      return true;
    }
    else
      return false;
  }
  

  // This method is for checking whether the student has gone through a course, and whether the grade point is below the fail grade point.
  public boolean isOndangerList(Student guy) {
    this.gradeCalc(guy);
    if (guy.getGPA() < FAIL_DEAN && guy.getNumOfCourses() > 0){
      return true;
    }
    else
      return false;
  }
  
  
  //Method used to compute the random ID for every student.
  public static int generateId(int year) {
    Random myRandGenerator;
    int randNumber;
    
    // Compute the ID in Ashesi's format
    myRandGenerator = new Random();
    randNumber = myRandGenerator.nextInt(); // generate a random number
    randNumber = randNumber % 10000; // ensure the random number has at most 4 digits
    randNumber = Math.abs(randNumber); // ensure the number is not negative
    int myId = randNumber*10000 + year; // create the ID in Ashesi's format
    
    return myId;
  }

//This is checking for whether the student is passing and doing extremely well.    
  public ArrayList<String> passCourses(){
    
    ArrayList<String> d =new ArrayList<String>();
    
    for (Map.Entry<String, String> entry : courseList.entrySet()) {
      String key = entry.getKey();
      String value = entry.getValue();
      
      if (value.equals("A")){
        d.add(key);
      }
    }
    return d;
  }
  
  
//This is checking for whether the student is failing and doing extremely poor.  
  public ArrayList<String> failCourses(){
    
    ArrayList<String> t =new ArrayList<String>();
    
    for (Map.Entry<String, String> entry : courseList.entrySet()) {
      String key = entry.getKey();
      String value = entry.getValue();
      
      if (value.equals("D")){
        t.add(key);
      }
      
      
    }
    return t;
  }
  
// This method is used to compute the grade point of a letter grade.  
  public void gradeCalc(Student n){
    
    ArrayList<Integer> f =new ArrayList<Integer>();
    
    for (Map.Entry<String, String> entry : courseList.entrySet()) {
      String key = entry.getKey();
      String value = entry.getValue();
      
      if (value.equals("A"))
        f.add(4);
      else if(value.equals("B"))
        f.add(3); 
      else if (value.equals("C"))
        f.add(2);
      else if (value.equals("D"))
        f.add(1);  
      
    }
    double gpa1=0;
    for (int i =0;i<f.size();i++){
      
      gpa1 += f.get(i);
    }
    gpa=gpa1/f.size();
    
  }
  
}