// A test class, where the student and database class are made use of.
// A student is created, given courses and added to the databases.


import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.ArrayList;


public class MyClass{
  
  public static void main(String[] args){
    
    DataBase database = new DataBase();
    
    
    //Students are created, filling the constructor with the various parameters.
    Student Pali = new Student("Pali",2018,"MIS","ABC");
    Student Nana = new Student("Nana",2018,"MIS","XYZ");
    
    
    //Student created is given a course, with the name of course as the key and the value being the letter grade
    Pali.addCourse("Stats", "A");
    Nana.addCourse("Leadership","D");
    //Student is added to the database class
    database.addStudent(Pali);
    database.addStudent(Nana); 
    
    //Query, designed to give responses based on whether the user is a student or administrator
    
    
    //The method passcourses is used to determine if Pali, actually has good grades in his courses.
   // System.out.println( database.getObject("Pali").passCourses());
    
    //The method passcourses is used to determine if Nana, actually has bad grades in his courses.   
    //System.out.println( database.getObject("Nana").failCourses());  
    
//    System.out.println(database.getDangerList());
    
    Scanner sc = new Scanner(System.in);
    System.out.println("Admin or Student ");
    
    String answer = sc.nextLine();
    if(answer.equalsIgnoreCase("student")){
      
      System.out.println("Sign up or login ");
      
      String answer1 = sc.nextLine();
      
      
      if (answer1.equalsIgnoreCase("Sign up")){
        
        database.loadMaps();
        
        System.out.println("Enter your name ");
        
        String username = sc.nextLine();
        
        System.out.println("password ");
        
        String password = sc.nextLine();
        
        System.out.println("Enter year group ");
        
        int ygroup = sc.nextInt();
        
        System.out.println("Enter your major ");
        
        String major = sc.nextLine();
        
        Student a =new Student(username,ygroup,major,password);
        System.out.println("This is a display of your info ");
        database.addStudent(a);
        database.print1(a);
        System.out.println("Enter your course ");
        
        int count =0;
        System.out.println("How many courses");
        int num = sc.nextInt();
        String course = sc.nextLine(); 
        while(count!=num) {
          
          course = sc.nextLine();
          a.addCourse(course);
          count++;
        }
        
        System.out.println("These are your courses ");
        a.getStudentCourse();
        
        System.out.println("Add friend name ");
        
        int count1 =0;
        System.out.println("How many friends");
        int num1 = sc.nextInt();
        String friend = sc.nextLine(); 
        while(count1!=num1) {
          
          friend = sc.nextLine();
          database.friend(a,database.getObject(friend));
          count1++;
        }
        
        System.out.println("His friends are:"+database.showf(a));     
        System.out.println("list of friends doing the same major ");
        List<String> b = database.showf(a);      
        List<String> c = new ArrayList<String>();
        
        for(int i =0;i<b.size();i++){
          
          if (database.getObject(b.get(i)).getMajor().equalsIgnoreCase(a.getMajor())){
            
            c.add(b.get(i));
          }
          System.out.println(c);   
        }
        
        //recomended course=course of senior friends which they got an A in
        System.out.println("recommended couses: ");
        List e = new ArrayList();
        for(int i =0;i<b.size();i++){
          
          
          
          if(database.getObject(b.get(i)).getYeargroup() < database.getObject(a.fullName).getYeargroup()){
            
            e.add(database.getObject(b.get(i)). passCourses());
          }
          System.out.println(e);
        }
        database.print1(a);
        database.saveMaps() ; 
      }
      
      else if(answer1.equalsIgnoreCase("login")){
        database.loadMaps();
        System.out.println("Enter username");
        
        String username1 = sc.nextLine();
        System.out.println("Password");
        String password1 = sc.nextLine();
        
        if(password1.equalsIgnoreCase(database.getObject(username1).getPassword())){ 
          System.out.println("Welcome Student");
           System.out.println("This is a display of your info ");
           database.print1(database.getObject(username1));
           System.out.println("Your friends are:"+database.showf(database.getObject(username1)));
           System.out.println("list of friends doing the same major ");
        List<String> z = database.showf(database.getObject(username1));      
        List<String> y = new ArrayList<String>();
        
        for(int i =0;i<z.size();i++){
          
          if (database.getObject(z.get(i)).getMajor().equalsIgnoreCase(database.getObject(username1).getMajor())){
            
            y.add(z.get(i));
          }
          System.out.println(y);   
        }
         System.out.println("recommended couses: ");
        List f = new ArrayList();
        for(int i =0;i<z.size();i++){
          
          
          
          if(database.getObject(z.get(i)).getYeargroup() < database.getObject(username1).getYeargroup()){
            
            f.add(database.getObject(z.get(i)). passCourses());
          }
          System.out.println(f);
        }
        }
      }
      database.saveMaps() ;
    }
    else if(answer.equalsIgnoreCase("admin"))
    {  
      System.out.println("Welcome admin");
      System.out.println("Here is a display of the various student information"); 
      database.print();
      
      System.out.println("do you want to see deans list");
      String answer2 = sc.nextLine();
      if(answer2.equalsIgnoreCase("yes")){
        System.out.println("Here is a display of the students on the deans list");
        System.out.println(database.getDeansList());
       // database.print();
      }
      
      System.out.println("do you want to see danger list");
      String answer7 = sc.nextLine();
      if(answer7.equalsIgnoreCase("yes")){
        System.out.println("Here is a display of the students on the danger list");
        System.out.println(database.getDangerList());
      }
      System.out.println("do you want to work on a student");   
      String answer3 = sc.nextLine(); 
      if(answer3.equalsIgnoreCase("yes")){
        System.out.println("whats his username"); 
        
        String answer4 = sc.nextLine();
        System.out.println("what course do you want to edit details ");
        String answer5 = sc.nextLine();
        System.out.println("For " + answer5 + " what grade do you want to give it");
        String answer99 = sc.nextLine();
        database.getObject(answer4).setCourse(answer5,answer99);
      }  
      
      database.print();  
      database.saveMaps() ;
    }
  }
}






